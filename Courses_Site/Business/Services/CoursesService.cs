﻿using System;
using System.Collections.Generic;
using Core.Entities;
using Data;
using System.Linq;

namespace Business.Services
{
    public class CoursesService : Service
    {
        private readonly IRepository<Courses> _courseRepo
            = new NHibernateRepository<Courses>();

        public void CreateCourse(string deffinition, string address,DateTime startdate,DateTime finishdate, Company company, List<Category> categories)
        {
            var course = new Courses(deffinition, address, startdate, finishdate, company, categories);
            _courseRepo.Save(course);
        }

        public IEnumerable<Courses> GetAll()
        {
            return _courseRepo.GetAll();
        }

        public IEnumerable<Courses> GetByIdCategory(int categoryId)
        {
            return _courseRepo.GetCoursesByCategotyId(categoryId);
        }

        public void DeleteCourses(int id)
        {
            _courseRepo.Delete(id);
        }

        
        public void CreateCourse(string diffinition, string adddress, DateTime start, DateTime finish, Category category,int userId)
        {
            var company = _courseRepo.GetCompanyByUserId(userId);
            var cats = new List<Category>();
            cats.Add(category);
            var course = new Courses(diffinition,adddress,start,finish,company,cats);
            _courseRepo.Save(course);
        }

        public Courses Get(int i)
        {
            return _courseRepo.Get(i);
        }
    }
}
