﻿using System;
using System.Collections.Generic;
using Core.Entities;
using Data;

namespace Business.Services
{
   public class CompaniesService:Service
    {
        private readonly IRepository<Company> _companyRepo
            = new NHibernateRepository<Company>();

        private UserService _userService = new UserService();
        private RoleService _roleService = new RoleService();
        private CoursesService _coursesService = new CoursesService();

        public void CreateCompany(string name, string address, DateTime data,User owner)
        {
            var cmp= new Company(name, data, address,owner);
            _companyRepo.Save(cmp);
        }

       public Company GetCompany(int a)
       {
           return _companyRepo.Get(a);
       }

       public Company GetCompanyByownerId(int id)
       {
           return _companyRepo.GetCompanyByownerId(id);
       }

       public IEnumerable<Company> GetAllCompanies()
       {
           return _companyRepo.GetAll();
       }

       public Company GetCompanyByUserId(int userId)
       {
           return _companyRepo.GetCompanyByUserId(userId);
       }

       public void DeleteCompany(int companyId, int userId)
       {
           Company company = this.GetCompany(companyId);
           //удаление всех курсов компании
           //поиск всех членов этой компании , изменение их роли на просто Юзер, изменение их компании на НАЛЛ
           foreach (var courses in company.Coursess)
           {
               _coursesService.DeleteCourses(courses.Id);
           }
           foreach (var user in company.Users)
           {
               user.Roles[0] = _roleService.Get(3);               
               _userService.Save(user);
           }
           if (_userService.GetUser(userId).Roles[0].RoleName.Equals("COwner"))
           {
               /*роль пользователя меня ется на юзер*/
               User user = company.Owner;
               user.Companies = null;
               user.Roles[0] = _roleService.Get(3);
               _userService.Save(user);
           }
               _companyRepo.Delete(companyId);
       }

       public void CreateCompany(string companyName, string address, DateTime dateTime, int OwnerId)
       {
           var owner = _userService.GetUser(OwnerId);
           owner.Roles[0] = _roleService.Get(1);
           owner.Company = this.GetCompanyByownerId(OwnerId);
           _userService.Save(owner);
           var cmp = new Company(companyName, dateTime, address, owner);
           _companyRepo.Save(cmp);
       }

       public void AddUserInCombany(int companyId, int userId)
       {
           Company comp = this.GetCompany(companyId);
           User owner = _userService.GetUser(userId);
           // изменение роли пользователя на члена компании
           owner.Roles[0] = _roleService.Get(2);
           // добывление пользователю компании или компании пользователя...
           owner.Company = comp;
           _userService.Save(owner);
            
       }
    }
}
