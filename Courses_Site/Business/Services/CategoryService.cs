﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Data;

namespace Business.Services
{
    public class CategoryService
    {
        private readonly IRepository<Category> _CategoryRepo
            = new NHibernateRepository<Category>();

        public IEnumerable<Category> GetAll()
        {
            return _CategoryRepo.GetAll();
        }

        public Category Get(int id)
        {
          return  _CategoryRepo.Get(id);
           
        }
    }
}
