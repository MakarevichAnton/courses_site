﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Data;

namespace Business.Services
{
     public class TagService
    {
        private readonly IRepository<Tag> _TagRepo
            = new NHibernateRepository<Tag>();

        public IEnumerable<Tag> GetAll()
        {
            return _TagRepo.GetAll();
        }

         public Tag Get(int selectCategory)
         {
            return _TagRepo.Get(selectCategory);
         }
    }
}
