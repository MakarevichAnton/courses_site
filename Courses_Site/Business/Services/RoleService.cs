﻿using Core.Entities;
using Data;

namespace Business.Services
{
    public class RoleService:Service
    {
        private readonly IRepository<Role> _RoleRepo
            = new NHibernateRepository<Role>();

        public System.Collections.Generic.IEnumerable<Role> GetAll()
        {
            return _RoleRepo.GetAll();
        }

        public Role Get(int id)
        {
          return  _RoleRepo.Get(id);
        }

        

        public void Save(Role role)
        {
            _RoleRepo.Save(role);
        }
    }
}
