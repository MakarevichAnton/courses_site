﻿using System.Collections.Generic;
using Core.Entities;
using Data;
using System;

namespace Business.Services
{
    public class UserService:Service
    {
        private readonly NHibernateRepository<User> _userRepo
            = new NHibernateRepository<User>();

        private RoleService _roleService = new RoleService();

        public User GetUser(int a)
        {
            return _userRepo.Get(a);
        }

        public User GetUserByName(string name)
        {
          return _userRepo.GetUserByName(name);

        }

        public void Save(User user)
        {
            _userRepo.Save(user);
        }

        public User GetUserById(int p)
        {
            return _userRepo.Get(p);
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepo.GetAll();
        }

        public IEnumerable<User> GetUserByIdOwner(int idOwner)
        {
            return _userRepo.GetCompanyMembersByOwnerId(idOwner);
        }

        public void DeleteUser(int id)
        {
            _userRepo.Delete(id);
        }

        public void DeleteUserFromCompany(int idUser)
        {
            var user = this.GetUser(idUser);
            user.Roles[0] = _roleService.Get(3);//изменяем роль пользователя на юзер
            user.Company = null;//делаем НУЛЛ в столбце компании пользователя
            this.Save(user);            
        }
    }
}