﻿using System.Collections.Generic;
using Core.Entities;
using Data;
using System;

namespace Business.Services
{
    public class NewsService:Service
    {
        private readonly IRepository<News> _newsRepo
            = new NHibernateRepository<News>();

        public void CreateNews( string title, string text, DateTime addDate,User user,Tag tag)
        {
            var tags = new List<Tag> {tag};
            var news = new News(title, text, addDate,user,tags);
            _newsRepo.Save(news);
        }

        public IEnumerable<News> GetAll()
        {
            return _newsRepo.GetAll();
        }

        public void DeleteNews(int id)
        {
            _newsRepo.Delete(id);
        }

        public IEnumerable<News> GetByIdTag(int p)
        {
            return _newsRepo.GetNewsByTagId(p);
        }
    }
}
