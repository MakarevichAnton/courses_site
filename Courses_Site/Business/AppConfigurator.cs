﻿using Data;

namespace Business
{
    public static class ApplicationConfigurator
    {
        public static void Configure()
        {
            Configurator.Configure();
        }
    }
}
