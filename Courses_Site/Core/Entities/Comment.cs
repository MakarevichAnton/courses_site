﻿

using System;

namespace Core.Entities
{
    public class Comment:EntityObject
    {
        public virtual int CommId { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Text { get; set; }
        public virtual int Type { get; set; }
        public virtual DateTime Date { get; set; }

        public Comment()
        {
        }

        public Comment(int id, string name, string text, int type, DateTime date)
        {
            CommId = id;
            UserName = name;
            Text = text;
            Type = type;
            Date = date;
        }
    }
}
