﻿

using System.Collections.Generic;

namespace Core.Entities

{
    public class Category:EntityObject
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<Courses> Courses { get; set; }   

        public Category()
        {
        }

        public Category(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
