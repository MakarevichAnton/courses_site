﻿
using System.Collections.Generic;

namespace Core.Entities
{
    public class Role : EntityObject
    {
        public virtual int RoleId { get; set; }
        public virtual string RoleName { get; set; }
        public virtual IList<User> Users { get; set; }  

        public Role()
        {
        }

        public Role(int id, string name)
        {
            RoleId = id;
            RoleName = name;
        }
    }
}
