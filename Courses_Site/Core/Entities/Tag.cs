﻿using System;
using System.Collections.Generic;

namespace Core.Entities
{
    public class Tag : EntityObject
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual IList<News> News { get; set; }  

        public Tag()
        {
        }

        public Tag(int id, string name)
        {
            Id = id;
            Title = name;
        }
    }
}