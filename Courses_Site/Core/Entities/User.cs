﻿
using System.Collections.Generic;

namespace Core.Entities
{
    public class User:EntityObject
    {
        public virtual int UserId { get; set; }
        public virtual string UserName { get; set; }
       // public virtual Role Role { get; set; }
        public virtual IList<Role> Roles { get; set; }
        public virtual Company Company { get; set; }
        public virtual IList<News> Newss { get; set; }
        public virtual IList<Company> Companies { get; set; }

        public User()
        {
        }

        public User(int id,string name)
        {
            UserId = id;
            UserName = name;
        }
    }
}
