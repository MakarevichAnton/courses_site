﻿using System;
using System.Collections.Generic;

namespace Core.Entities
{
    public class Courses : EntityObject
    {
       // private string diffinition;
      //  private string adddress;
       // private DateTime start;
      //  private DateTime finish;

        public virtual int Id { get; set; }
        public virtual string Deffinition { get; set; }
        public virtual string Address { get; set; }
        public virtual DateTime StartAt { get; set;}
        public virtual DateTime FinishAt { get; set; }
        public virtual IList<Category> Categories { get; set; }
        public virtual Company Company { get; set; }

        public Courses()
        {
        }

        public Courses( string deff, string address,DateTime datestart,DateTime datefinish,Company company,List<Category> categories )
        {
            Deffinition = deff;
            Address = address;
            StartAt = datestart;
            FinishAt = datefinish;
            Company = company;
            Categories = categories;

        }

        public Courses(string diffinition, string adddress, DateTime start, DateTime finish)
        {
            // TODO: Complete member initialization
         //   this.diffinition = diffinition;
          //  this.adddress = adddress;
          //  this.start = start;
          //  this.finish = finish;
        }
    }
}