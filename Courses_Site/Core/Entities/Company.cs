﻿
using System;
using System.Collections.Generic;

namespace Core.Entities
{
    public class Company : EntityObject
    {
        public virtual int ID { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual DateTime RegDate { get; set; }
        public virtual string Address { get; set; }
        public virtual IList<Courses> Coursess { get; set; }
        public virtual IList<User> Users { get; set; }
        public virtual User Owner { get; set; }

        public Company()
        {
        }

        public Company(string name,DateTime regdate,string address,User owner)
        {
            CompanyName = name;
            RegDate = regdate;
            Address = address;
            Owner = owner;
        }
    }
}