﻿using System;
using System.Collections.Generic;

namespace Core.Entities
{
    public class News : EntityObject
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Text { get; set; }
        public virtual DateTime AddDate { get; set; }
        public virtual IList<Tag> Tags { get; set; }
        public virtual User User { get; set; }

        public News()
        {
        }

        public News( string title, string text, DateTime addDate,User user ,List<Tag> tags )
        {
            Title = title;
            Text = text;
            AddDate = addDate;
            Tags = tags;
            User = user;
        }

        public News(string title, string text, DateTime addDate)
        {
            Title = title;
            Text = text;
            AddDate = addDate;
        }
    }
}