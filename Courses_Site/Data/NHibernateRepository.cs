﻿using System.Collections;
using System.Collections.Generic;
using Core.Entities;
using NHibernate;
using Core;
using System.Linq;

namespace Data
{
    public class NHibernateRepository<T> : Repository<T>
        where T : EntityObject
    {
        protected readonly ISession Session;

        public NHibernateRepository()
        {
            Configurator.Configure();
            Session = Configurator.OpenSession();
        }

        public override T Get(int id)
        {
            return Session.Get<T>(id);
        }

        public override IEnumerable<T> GetAll()
        {
            return Session.QueryOver<T>().List();
        }

        public override void Save(T entity)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Merge(entity);
                transaction.Commit();
            }
        }

        public override void Delete(int id)
        {
            var entity = Get(id);
            Delete(entity);
        }

        public override void Delete(T entity)
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.Delete(entity);
                transaction.Commit();
            }
        }

        public override void DeleteAll()
        {
            using (var transaction = Session.BeginTransaction())
            {
                Session.CreateQuery("delete from " + typeof(T).Name);
                transaction.Commit();
            }
        }

        public override IEnumerable<Courses> GetCoursesByCategotyId(int id)
        {
                using (var transaction = Session.BeginTransaction())
                {
                   return Session.QueryOver<Courses>()
                           .JoinQueryOver<Category>(x => x.Categories)
                           .Where(x => x.Id == id)
                           .List<Courses>();
                }
        }

        public override Company GetCompanyByUserId(int id)
        {
            using (var transaction = Session.BeginTransaction())
            {
                User user= Session.QueryOver<User>()
                                      .Where(x => x.UserId == id)
                                      .List<User>()
                                      .FirstOrDefault();

                /*Company company = Session.QueryOver<Company>()
                                         .Where(x => x.Owner == user)
                                         .List<Company>()
                                         .FirstOrDefault();*/
                return user.Company;

            }
        }

        public override User GetUserByName(string name)
        {
            using (var transaction = Session.BeginTransaction())
            {
                return Session.QueryOver<User>()
                        .Where(x => x.UserName == name)
                        .List<User>().FirstOrDefault();
            }
        }

        public override IEnumerable<News> GetNewsByTagId(int p)
        {
            using (var transaction = Session.BeginTransaction())
            {
                IList<News> news = Session.QueryOver<News>()
                                          .JoinQueryOver<Tag>(x => x.Tags)
                                          .Where(x => x.Id == p)
                                          .List<News>();
                return news;
            }
        }

        public override IEnumerable<User> GetCompanyMembersByOwnerId(int ownerId)
        {
            using (var transaction = Session.BeginTransaction())
            {
                User user = Session.QueryOver<User>().Where(x => x.UserId == ownerId).List<User>().FirstOrDefault();
                Company xx = user.Company;
                var n = Session.QueryOver<User>()
                               .Where(x => x.Company == xx)
                               .List<User>();
                
                return n;
            }
        }


        public override Company GetCompanyByownerId(int id)
        {
            using (var transaction = Session.BeginTransaction())
            {
                var user = Session.QueryOver<User>()
                                  .Where(x => x.UserId == id).List<User>().FirstOrDefault();
                return Session.QueryOver<Company>()
                              .Where(x => x.Owner == user)
                              .List<Company>()
                              .FirstOrDefault();
            }
        }
    }
}