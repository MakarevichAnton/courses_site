﻿using Data.Mappings;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace Data
{
    public static class Configurator
    {
        private static FluentConfiguration _config;
        private static ISessionFactory _sessionFactory;

        public static void Configure()
        {
            _config = Fluently
                .Configure()
                .Database(MsSqlConfiguration
                              .MsSql2008
                              //.ConnectionString(@"Data Source=. \sqlexpress;Initial Catalog=Courses;Integrated Security=True;Pooling=False"))
                              .ConnectionString(@"Data Source=ANUTA-PC;Initial Catalog=Courses;Integrated Security=True;Pooling=False"))                             
                .Mappings(configuration => configuration.FluentMappings.AddFromAssemblyOf<UserMap>());

            _sessionFactory = _config.BuildSessionFactory();
        }

        public static ISession OpenSession()
        {
            return _sessionFactory.OpenSession();
        }
    }


}
