﻿using System.Collections.Generic;
using Core;
using Core.Entities;


namespace Data
{

    public interface IRepository<T>
        where T : EntityObject
    {
        T Get(int id);

        IEnumerable<T> GetAll();

        void Save(T entity);

        void Save(IEnumerable<T> entities);

        void Delete(int id);

        void Delete(T entity);

        void DeleteAll();

        IEnumerable<Courses> GetCoursesByCategotyId(int id);
        Company GetCompanyByUserId(int id);
        User GetUserByName(string name);
        IEnumerable<News> GetNewsByTagId(int p);
        IEnumerable<User> GetCompanyMembersByOwnerId(int ownerId);
        
        Company GetCompanyByownerId(int id);
    }
}
