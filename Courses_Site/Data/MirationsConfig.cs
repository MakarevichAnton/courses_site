﻿using System.Reflection;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;


namespace Data
{
    public class MirationsConfig
    {
        public static void Run()
        {
           // const string connectionString = @"Data Source=. \sqlexpress;Initial Catalog=Courses;Integrated Security=True;Pooling=False";
            const string connectionString = @"Data Source=ANUTA-PC;Initial Catalog=Courses;Integrated Security=True;Pooling=False";
            
            var assemly = Assembly.GetExecutingAssembly();
            Announcer announcer = new TextWriterAnnouncer(System.Console.WriteLine);
            var factory = new SqlServer2008ProcessorFactory();
            IRunnerContext migrationContext = new RunnerContext(announcer)
            {
                Namespace = "Data.Migrations"
            };

            var options = new ProcessorOptions
            {
                PreviewOnly = false, // set to true to see the SQL
                Timeout = 60
            };
            IMigrationProcessor processor = factory.Create(connectionString, announcer, options);
            var runner = new MigrationRunner(assemly, migrationContext, processor);
            runner.MigrateUp(true);
            runner.ListMigrations();

        }
    }
}
