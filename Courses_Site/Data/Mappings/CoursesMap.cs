﻿using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class CoursesMap : ClassMap<Courses>
    {
        public CoursesMap()
        {
            Table("Courses");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Deffinition);
            Map(x =>x.Address);
            Map(x => x.StartAt);
            Map(x => x.FinishAt);
           
            References(x => x.Company)
                .ForeignKey("Company_id")
                .Cascade.None();
                
               
            HasManyToMany(x => x.Categories)
                .Table("Courses_Categories")                
                .ParentKeyColumn("CoursesId")
                .ChildKeyColumn("CategoryId")
                .Cascade.SaveUpdate()
                .AsBag();


        }
    }
}
