﻿

using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class CommentMap:ClassMap<Comment>
    {
        public CommentMap()
        {
            Table("Comment");
            Id(x => x.CommId).GeneratedBy.Native();
            Map(x => x.Text);
            Map(x => x.UserName);
            Map(x => x.Type);
            Map(x => x.Date);
        }
    }
}
