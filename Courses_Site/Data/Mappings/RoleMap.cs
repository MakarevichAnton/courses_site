﻿using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class RoleMap : ClassMap<Role>
    {
        public RoleMap()
        {
            Table("webpages_Roles");
            Id(x => x.RoleId).GeneratedBy.Native();
            Map(x => x.RoleName);


            HasManyToMany(x => x.Users)
                .Table("webpages_UsersInRoles")              
                .ParentKeyColumn("RoleId")
                .ChildKeyColumn("UserId")
                .Cascade.SaveUpdate()
                .AsBag();
        }
    }
}
