﻿using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class UserMap:ClassMap<User>
    {
        public UserMap()
        {
            Table("UserProfile");
            Id(x => x.UserId).GeneratedBy.Native();
            Map(x => x.UserName);     

            References(x => x.Company)
                .ForeignKey("Company_id")
                .Cascade.None();

            HasManyToMany(x =>x.Roles)
                .Table("webpages_UsersInRoles")
                .ParentKeyColumn("UserId")
                .ChildKeyColumn("RoleId" )
                .Cascade.SaveUpdate()
                .AsBag();


            HasMany(x => x.Newss)
                .Inverse()
                .AsBag()
                .KeyColumn("user_Id")
                .Cascade.SaveUpdate();

            HasMany(x => x.Companies)
                .Inverse()
                .AsBag()
                .KeyColumn("Owner")
                .Cascade.SaveUpdate();
           
        }
    }
}
