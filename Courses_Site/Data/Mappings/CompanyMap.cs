﻿using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class CompanyMap:ClassMap<Company>
    {
        public CompanyMap ()
        {
            Table("Company");
            Id(x => x.ID).GeneratedBy.Native();
            Map(x => x.CompanyName);
           
            Map(x => x.RegDate);
            Map(x => x.Address);

            HasMany(x => x.Coursess)
                .Inverse()
                .AsBag()
                .KeyColumn("Company_ID")
                .Cascade.All();

            HasMany(x => x.Users)
                .Inverse()
                .AsBag()
                .KeyColumn("Company_ID")
                .Cascade.SaveUpdate();

            References(x => x.Owner).Column("Owner").Cascade.None();
        }
    }
}
