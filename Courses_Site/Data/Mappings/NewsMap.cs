﻿using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class NewsMap : ClassMap<News>
    {
        public NewsMap()
        {
            Table("News");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Title);
            Map(x => x.Text);
            Map(x => x.AddDate);

            HasManyToMany(x => x.Tags)
                .Table("News_Tags")
                .ParentKeyColumn("NewsId")
                .ChildKeyColumn("TagId")
                .Cascade.SaveUpdate()
                .AsBag();

            References(x => x.User).ForeignKey("user_Id").Cascade.None();
        }
    }
}
