﻿

using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class TagMap:ClassMap<Tag>
    {
        public TagMap()
        {
            Table("Tag");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Title);

            HasManyToMany(x => x.News)
                .Table("News_Tags")
                .Inverse()
                .ParentKeyColumn("TagId")
                .ChildKeyColumn("NewsId")
                .Cascade.SaveUpdate()
                .AsBag();
        }
    }
}
