﻿
using Core.Entities;
using FluentNHibernate.Mapping;

namespace Data.Mappings
{
    class CategoryMap:ClassMap<Category>
    {
        public CategoryMap()
        {
            Table("Category");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Name);



            HasManyToMany(x => x.Courses)
                .Table("Courses_Categories")
                .Inverse()
                .ParentKeyColumn("CategoryId")
                .ChildKeyColumn("CoursesId")
                .Cascade.SaveUpdate()
                .AsBag();
        }
    }
}
