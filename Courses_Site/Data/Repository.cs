﻿using System.Collections.Generic;
using Core;
using Core.Entities;

namespace Data
{
    public abstract class Repository<T> : IRepository<T>
         where T : EntityObject
    {
        public abstract T Get(int id);
        public abstract IEnumerable<T> GetAll();

        public abstract void Save(T entity);

        public void Save(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Save(entity);
            }
        }
        public abstract void Delete(int id);
        public abstract void Delete(T entity);
        public abstract void DeleteAll();

        public abstract IEnumerable<Courses> GetCoursesByCategotyId(int id);
        public abstract Company GetCompanyByUserId(int id);
        public abstract User GetUserByName(string name);
        public abstract IEnumerable<News> GetNewsByTagId(int p);
      //  public abstract Company GetCompanyById(int idCompany);
        public abstract IEnumerable<User> GetCompanyMembersByOwnerId(int ownerId);
      
        public abstract Company GetCompanyByownerId(int id);
    }
}
