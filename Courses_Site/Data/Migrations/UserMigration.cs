﻿using FluentMigrator;

namespace Data.Migrations
{

    [Migration(2013072620)]
    public class UserMigration : Migration
    {
        public override void Up()
        {
            Alter.Table("UserProfile")
                 .AddColumn("Company_id").AsInt32().Nullable()               
                 .AddColumn("Accessed").AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.FromTable("News");
            Delete.Table("News");
        }
    }
    
}



