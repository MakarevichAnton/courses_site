﻿using FluentMigrator;

namespace Data.Migrations
{
    [Migration(2013072615)]
    public class CoursesCategoriesMigration:Migration
    {
        public override void Up()
        {
            Create.Table("Courses_Categories")
                  .WithColumn("CoursesID").AsInt32().NotNullable().ForeignKey("Courses","ID")
                  .WithColumn("CategoryID").AsInt32().NotNullable().ForeignKey("Category","Id");

        }

        public override void Down()
        {
            Delete.FromTable("Courses_Categories");
            Delete.Table("Courses_Categories");
        }
    }
}
