﻿using FluentMigrator;

namespace Data.Migrations
{

    [Migration(2013072622)]
    public class CommentsMigration : Migration
    {
        public override void Up()
        {
            Create.Table("Comment")
                  .WithColumn("CommId")
                  .AsInt32()
                  .NotNullable()
                  .PrimaryKey("CommId")
                  .Identity()
                  .WithColumn("Type")
                  .AsInt32()
                  .NotNullable()
                  .WithColumn("Text")
                  .AsString(1000)
                  .NotNullable()
                  .WithColumn("UserName")
                  .AsString(250)
                  .NotNullable()
                  .WithColumn("Date")
                  .AsDateTime()
                  .NotNullable();


        }

        public override void Down()
        {
            Delete.FromTable("Comment");
            Delete.Table("Comment");
        }
    }
}