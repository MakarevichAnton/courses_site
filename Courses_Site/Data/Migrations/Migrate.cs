﻿using FluentMigrator;

namespace Data.Migrations
{
    [Migration(2013072603)]
    public class CompanyMigration : Migration
    {
        public override void Up()
        {
           
            Create.Table("Company")
                  .WithColumn("ID")
                  .AsInt32()
                  .NotNullable()
                  .PrimaryKey("CompanyID")
                  .Identity()
                  .WithColumn("CompanyName")
                  .AsString(255)
                  .NotNullable()
                  .WithDefaultValue(string.Empty)
                  .WithColumn("Address")
                  .AsString(255)
                  .NotNullable()
                  .WithDefaultValue(string.Empty)
                  .WithColumn("RegDate")
                  .AsDateTime()
                  .NotNullable()
                  .WithColumn("Owner")
                  .AsInt32()
                  .NotNullable()
                  .ForeignKey("UserProfile", "Userid")
                  .WithColumn("Acseessed")
                  .AsInt32()
                  .NotNullable()
                  .WithDefaultValue("0");
 ;
        }

        public override void Down()
        {
            Delete.FromTable("Company");
            Delete.Table("Company");
        }
    }

    [Migration(2013072604)]
    public class CoursesMigration : Migration
    {
        public override void Up()
        {
            Create.Table("Courses")
                  .WithColumn("ID").AsInt32().NotNullable().PrimaryKey("CrsID").Identity()
                  .WithColumn("Deffinition").AsString(1000).NotNullable().WithDefaultValue(string.Empty)
                  .WithColumn("Address").AsString(255).NotNullable()
                  .WithColumn("StartAt").AsDateTime().NotNullable()
                  .WithColumn("FinishAt").AsDateTime().Nullable()
                  .WithColumn("Company_id").AsInt32().NotNullable()
                  .ForeignKey("Company","ID");
        }

        public override void Down()
        {
            Delete.FromTable("Courses");
            Delete.Table("Courses");
        }
    }

    
}

