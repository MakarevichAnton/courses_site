﻿using System;
using FluentMigrator;

namespace Data.Migrations
{
    [Migration(2013072619)]
     class ReferencesMigratin:Migration
    {
        public override void Up()
        {

            Alter.Table("Company")
                 .AlterColumn("User_id")
                 .AsInt32()
                 .NotNullable()
                 .ReferencedBy("UserProfile", "UserId");
        }

        public override void Down()
        {
            
        }
    }
}
