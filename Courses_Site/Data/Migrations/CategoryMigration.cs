﻿using FluentMigrator;

namespace Data.Migrations
{
   
    [Migration(2013072613)]
   public class CategoryMigration:Migration
    {
        public override void Up()
        {
            Create.Table("Category")
                  .WithColumn("Id")
                  .AsInt32()
                  .NotNullable()
                  .PrimaryKey("CatId")
                  .Identity()
                  .WithColumn("Name")
                  .AsString(255)
                  .NotNullable();
        }

        public override void Down()
        {
            Delete.FromTable("Category");
            Delete.Table("Category");
        }
    }
}
