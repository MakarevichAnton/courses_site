﻿using FluentMigrator;

namespace Data.Migrations
{
    [Migration(2013072608)]
    public class NewsMigration : Migration
    {
        public override void Up()
        {
            Create.Table("News")
                  .WithColumn("ID")
                  .AsInt32()
                  .NotNullable()
                  .PrimaryKey("NewsId")
                  .Identity()
                  .WithColumn("Title")
                  .AsString(50)
                  .NotNullable()
                  .WithColumn("Text")
                  .AsString()
                  .NotNullable()
                  .WithColumn("addDate")
                  .AsDateTime()
                  .NotNullable()
                  .WithColumn("User_id")
                  .AsInt32().NotNullable().ForeignKey("UserProfile", "UserId");
        }

        public override void Down()
        {
            Delete.FromTable("News");
            Delete.Table("News");
        }
    }
}
