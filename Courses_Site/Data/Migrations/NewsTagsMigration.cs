﻿

using FluentMigrator;

namespace Data.Migrations
{
    [Migration(2013072618)]
    public class NewsTagsMigration:Migration
    {
        public override void Up()
        {
            Create.Table("News_Tags")
                  .WithColumn("NewsID").AsInt32().NotNullable().ForeignKey("News","ID")
                  .WithColumn("TagId").AsInt32().NotNullable().ForeignKey("Tag","ID");
        }

        public override void Down()
        {
            Delete.FromTable("News_Tags");
            Delete.Table("News_Tags");
        }
    }
}
