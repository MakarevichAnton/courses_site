﻿using FluentMigrator;

namespace Data.Migrations
{
    [Migration(2013072609)]   
    public class TagsMigration : Migration
    {
        public override void Up()
        {
            Create.Table("Tag")
                  .WithColumn("Id")
                  .AsInt32()
                  .NotNullable()
                  .PrimaryKey("TagId")
                  .Identity()
                  .WithColumn("Title")
                  .AsString(50)
                  .NotNullable();
        }

        public override void Down()
        {
            Delete.FromTable("Tag");
            Delete.Table("Tag");
        }
    }
}
