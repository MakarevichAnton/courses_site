﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Entities;


namespace Courses_Site.Models
{
    public class CompanyModels
    {
        public IEnumerable<Company> Companies { get; set; }
        public Company MyCompany { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
    }
}