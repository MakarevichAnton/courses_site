﻿using System.Collections.Generic;
using Core.Entities;


namespace Courses_Site.Models
{
    public class NewsModels
    {
        public IEnumerable<News> News { get; set; }
        public IEnumerable<Tag> Tags { get; set; }//для вывода на странице слева, мол самые популярные
        public IEnumerable<Tag> AllTag { get; set; }//для выбора при добавлении
        public int Page { get; set; }
        public int TotalPages { get; set; }
    }
}