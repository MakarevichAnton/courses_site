﻿using System.Collections.Generic;
using Core.Entities;


namespace Courses_Site.Models
{
    public class CourseModels
    {
        public IEnumerable<Courses> Courses { get; set; }
        public IEnumerable<Category> Categorys { get; set; }//для вывода на странице слева, мол самые популярные
        public IEnumerable<Category> AllCategories { get; set; }//для выбора при добавлении
        public int Page { get; set; }
        public int TotalPages { get; set; }
    }
}