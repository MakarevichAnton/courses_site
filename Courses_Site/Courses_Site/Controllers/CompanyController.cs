﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Business.Services;
using Core.Entities;
using Courses_Site.Models;
using WebMatrix.WebData;

namespace Courses_Site.Controllers
{
    public class CompanyController : Controller
    {   
        private  readonly  CompaniesService _companiesService = new CompaniesService();
        private  readonly  UserService _userService = new UserService();
        private readonly RoleService _roleService = new RoleService();

        public ActionResult Company(int? companyId,int? page)
        {
            var model = new CompanyModels();
           
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                if (_userService.GetUser(WebSecurity.CurrentUserId).Company!= null)
                model.MyCompany = _companiesService.GetCompanyByUserId(WebSecurity.CurrentUserId);
            }
            if (companyId.HasValue)
            {
                Company comp = _companiesService.GetCompany(companyId.Value);
                model.Companies = new List<Company> { comp };
            }
            else
            {
                model.Companies = _companiesService.GetAllCompanies();
            }
            if (page == null) { model.Page = 0; }
            else { model.Page = (int)page; }
            model.TotalPages = model.Companies.Count() / 5;
            int seq = model.Page * 5;
            if (model.Page != model.TotalPages)
            {
                var xx = model.Companies.ToArray();
                var xu = new Company[5];
                Array.Copy(xx, seq, xu, 0, 5);
                model.Companies = xu;
            }
            else
            {
                int last = model.Companies.Count() - seq;
                var xx = model.Companies.ToArray();
                var xu = new Company[last];
                Array.Copy(xx, seq, xu, 0, last);
                model.Companies = xu;
            }

            return View(model);
        }        

        public ActionResult Update()
        {
            return RedirectToAction("Company");
        }

        public ActionResult Delete(int id)
        {
            _companiesService.DeleteCompany(id, WebSecurity.CurrentUserId);
            return RedirectToAction("Company");
        }

        public ActionResult Create(string companyName, string address)
        {
            _companiesService.CreateCompany(companyName, address, DateTime.Now, WebSecurity.CurrentUserId);      
           return RedirectToAction("Company");
        }

        public ActionResult AddInCombany(int companyId)
        {
            _companiesService.AddUserInCombany(companyId, WebSecurity.CurrentUserId);
           return RedirectToAction("Company");
        }

        public ActionResult OpenPageCompany(int idCompany)
        {
            var model = new CompanyModels();
            model.MyCompany = _companiesService.GetCompany(idCompany);
            return View(model);
        }

        public ActionResult DeleteFrom()
        {
            _userService.DeleteUserFromCompany(WebSecurity.CurrentUserId);
            return RedirectToAction("Company");
        }

        public ActionResult DeleteInComрany(int idUser)
        {
            _userService.DeleteUserFromCompany(idUser);
            return RedirectToAction("Company");
        }
    }
}
