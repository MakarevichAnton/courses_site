﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Business.Services;
using Core.Entities;
using Courses_Site.Models;
using Courses_Site.Filters;
using WebMatrix.WebData;

namespace Courses_Site.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/
        private readonly UserService _userService = new UserService();
        private readonly RoleService _roleService = new RoleService();
        private  readonly  CompaniesService _companiesService = new CompaniesService();

        [Authorize(Roles="Admin,COwner")]
        public ActionResult Users(int? page)
        {
            UserModel model;
         
            if (Roles.GetRolesForUser(WebSecurity.CurrentUserName).Contains("COwner"))
            {
                model = new UserModel
                    {
                        Users = _userService.GetUserByIdOwner(WebSecurity.CurrentUserId)
                    };
            }

            else 
            {
                model = new UserModel
                    {
                        Users = _userService.GetAll()
                    };
            }
            if (page == null) { model.Page = 0; }
            else { model.Page = (int)page; }
            model.TotalPages = model.Users.Count()/10;
            int seq = model.Page * 10;
            if (model.Page != model.TotalPages)
            {
                var xx = model.Users.ToArray();
                var xu = new User[10];
                Array.Copy(xx, seq, xu, 0,10);
                model.Users = xu;
            }
            else
            {
                int last = model.Users.Count() - seq;
                var xx = model.Users.ToArray();
                var xu = new User[last];
                Array.Copy(xx, seq, xu, 0, last);
                model.Users = xu;
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _userService.DeleteUser(id);
            return RedirectToAction("Users");
        }


        public ActionResult DeleteInComрany(int id)
        {
            _userService.DeleteUserFromCompany(id);
            return RedirectToAction("Users");
        }
    }
}
