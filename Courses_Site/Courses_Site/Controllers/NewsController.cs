﻿using System;
using System.Linq;
using System.Web.Mvc;
using Business.Services;
using Core.Entities;
using Courses_Site.Models;
using WebMatrix.WebData;


namespace Courses_Site.Controllers
{
    public class NewsController : Controller
    {
        private readonly NewsService _newsService = new NewsService();
        private readonly UserService _userService = new UserService();
        private readonly TagService _tagService = new TagService();
        
        public ActionResult News(int? tagId,int? page)
        {
            NewsModels model;
            if (tagId.HasValue)
            {
                model = new NewsModels
                {
                    News = _newsService.GetByIdTag(tagId.Value),
                    Tags = _tagService.GetAll(),
                    AllTag = _tagService.GetAll()
                };
            }
            else
            {
            model = new NewsModels
            {
                News = _newsService.GetAll(),
                AllTag = _tagService.GetAll(),
                Tags = _tagService.GetAll(),
            };
             }
            if (page == null) { model.Page = 0; }
            else { model.Page = (int)page; }
            model.TotalPages = model.News.Count() / 5;
            int seq = model.Page * 5;
            if (model.Page != model.TotalPages)
            {
                var xx = model.News.ToArray();
                var xu = new News[5];
                Array.Copy(xx, seq, xu, 0, 5);
                model.News = xu;
            }
            else
            {
                int last = model.News.Count() - seq;
                var xx = model.News.ToArray();
                var xu = new News[last];
                Array.Copy(xx, seq, xu, 0, last);
                model.News = xu;
            }

            return View(model);
        }

        public ActionResult Update()
        {
            return RedirectToAction("News");
        }

        public ActionResult Delete(int id)
        {
            _newsService.DeleteNews(id);
            return RedirectToAction("News");
        }

        public ActionResult Create(string title, string text, int selectCategory)
        {
           var tag = _tagService.Get(selectCategory);
           var user = _userService.GetUser(WebSecurity.CurrentUserId);
           _newsService.CreateNews(title, text, DateTime.Now,user,tag);
            return RedirectToAction("News");
        }
    }
}
