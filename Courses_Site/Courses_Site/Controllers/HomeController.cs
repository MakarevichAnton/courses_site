﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Business.Services;
using Core.Entities;
using Courses_Site.Models;
using WebMatrix.WebData;

namespace Courses_Site.Controllers
{
    public class HomeController : Controller
    {
        private  readonly  NewsService _newsServ = new NewsService();
        private  readonly  UserService _userService = new UserService();
        private  readonly  CompaniesService _companiesService = new CompaniesService();
        private  readonly  CoursesService _coursesService = new CoursesService();
        private readonly CategoryService _categoryService = new CategoryService();
        private readonly RoleService _roleService = new RoleService();

        public ActionResult Index(int? categoryId,int? page)
        {
         Business.MigrationsRunner.RunUp();
            CourseModels model;            
            if (categoryId.HasValue)               
            {
                model = new CourseModels
                    { Courses = _coursesService.GetByIdCategory(categoryId.Value)};     
            }
            else           
             { 
                model = new CourseModels
                    { Courses = _coursesService.GetAll(),};
            }
            if (page == null) { model.Page = 0; }
            else { model.Page = (int)page; }
            model.Categorys = _categoryService.GetAll();
            model.AllCategories = _categoryService.GetAll();    
            model.TotalPages = model.Courses.Count()/5;
            int seq=model.Page*5;
            if (model.Page != model.TotalPages)
            {              
                var xx = model.Courses.ToArray();
                var xu = new Courses[5];
                Array.Copy(xx, seq, xu, 0, 5);
                model.Courses = xu;
            }
            else
            {
                int last = model.Courses.Count() - seq;
                var xx = model.Courses.ToArray();
                var xu = new Courses[last];
                Array.Copy(xx, seq, xu, 0, last);
                model.Courses = xu;
            }
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "О нашем сайте.";
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        public ActionResult Companies()
        {
            ViewBag.Message = "Companies";
            var model = new CompaniesModel{
                Companies= _companiesService.GetAllCompanies() 
            };  
            return View(model);
        }
       
        public ActionResult News()
        {
            ViewBag.Message = "News";
            return View();
        }

       public ActionResult Update()
       {
           return RedirectToAction("Index");
       }

       public ActionResult Delete(int id)
       {
           _coursesService.DeleteCourses(id);
           return RedirectToAction("Index");
       }

       public ActionResult Create(string diffinition, string adddress, String startStr, String finishStr, int selectCategory)
       {
           DateTime start = new DateTime(int.Parse(startStr.Split('/')[2]),
               int.Parse(startStr.Split('/')[0]),
              int.Parse(startStr.Split('/')[1]));
           DateTime finish = new DateTime(int.Parse(finishStr.Split('/')[2]),
              int.Parse(finishStr.Split('/')[0]),
             int.Parse(finishStr.Split('/')[1]));
           Category category= _categoryService.Get(selectCategory);
           _coursesService.CreateCourse(diffinition, adddress, start,finish,category,WebSecurity.CurrentUserId);
           return RedirectToAction("Index");
       }
    }
}
